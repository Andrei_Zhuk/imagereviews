<?php

class Api_Imagereviews_Notes extends Api_Service_Abstract
{

    private $_websiteHelper = null;

    /**
     * Container mapper
     *
     * @var Application_Model_Mappers_ContainerMapper
     */
    private $_mapper = null;
    private $_notesMapper;

    protected $_accessList = array(
        Tools_Security_Acl::ROLE_USER       => array('allow' => array('get', 'post', 'put', 'delete')),
        Tools_Security_Acl::ROLE_SUPERADMIN => array('allow' => array('get', 'post', 'put', 'delete')),
        Tools_Security_Acl::ROLE_ADMIN      => array('allow' => array('get', 'post', 'put', 'delete'))
    );

    public function init()
    {
        $this->_websiteHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('website');
        $this->_mapper        = Application_Model_Mappers_ContainerMapper::getInstance();
        $this->_notesMapper   = Imagereviews_Mappers_ListMapper::getInstance();
    }

    public function getAction()
    {

    }

    public function postAction()
    {

    }

    public function deleteAction()
    {
        $data = Zend_Json::decode($this->_request->getRawBody());
        $id   = filter_var($data['id'], FILTER_SANITIZE_NUMBER_INT);
        if ($id) {
            $note         = $this->_notesMapper->find($id);
            $deleteResult = $this->_notesMapper->delete($note);
            return $deleteResult;
        }
        return $id;
    }


    public function putAction()
    {
        $data    = Zend_Json::decode($this->_request->getRawBody());
        $id      = $data['id'];
        $message = $data['newMessage'];

        if ($id) {
            $comment = $this->_commentsMapper->find($id);
            if ($comment) {


                if ($message) {
                    $comment->setMessage($message);
                } else {
                    $comment->setPublished(1);
                }


                $result = $this->_commentsMapper->save($comment);
                if ($result) {
                    $message = array(
                        'responseText' => "Everything is okay!",
                        'class'        => 'success',
                        'error'        => false,
                    );
                } else {
                    $message = array(
                        'responseText' => "Something went wrong!",
                        'class'        => 'error',
                        'error'        => false,
                    );
                }
                return $message;
            }
        }
    }
}