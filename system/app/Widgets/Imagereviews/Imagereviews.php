<?php

class Widgets_Imagereviews_Imagereviews extends Widgets_AbstractContent
{

    protected function _init()
    {
        $this->_websiteHelper = Zend_Controller_Action_HelperBroker::getExistingHelper('website');
        $this->_sessionHelper = Zend_Controller_Action_HelperBroker::getExistingHelper('session');

        $this->_view = new Zend_View(array('scriptPath' => __DIR__ . '/views'));
        $this->_view->setHelperPath(APPLICATION_PATH . '/views/helpers/');
        $this->_view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
        $this->_view->addScriptPath($this->_websiteHelper->getPath() . 'seotoaster_core/application/views/scripts/');

        $this->_currentUser = $this->_sessionHelper->getCurrentUser();
        $this->_isAdmin     = Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_CONTENT);
        $this->_isUser      = $this->_currentUser->getRoleId() != Tools_Security_Acl::ROLE_GUEST ? true : false;
        $this->_isGuest     = $this->_currentUser->getRoleId() == Tools_Security_Acl::ROLE_GUEST ? true : false;
    }

    protected function _load()
    {
        if (empty($this->_options)) {
            throw new Exceptions_SeotoasterWidgetException(
                $this->_translator->translate(
                    'Not enough parameters for the widget.'
                )
            );
        }

        $methodName = '_render' . ucfirst(array_shift($this->_options));
        if (method_exists($this, $methodName)) {
            return $this->$methodName();
        } else {
            return 'Sorry. It is wrong name of widget.';
        }
    }

    protected function _renderCreate()
    {
        $this->_view->pageId = $this->_toasterOptions['id'];


        return $this->_view->render('button.phtml');
    }


    protected function _renderImage()
    {
        $this->_view->notes = Imagereviews_Mappers_ListMapper::getInstance()->fetchAll(
            array('pageId = ?' => $this->_toasterOptions['id'])
        );
        $this->_view->pageId = $this->_toasterOptions['id'];


        return $this->_view->render('image.phtml');
    }


    protected function _renderList()
    {
//        Imagereviews_Mappers_ListMapper::getInstance();

//        print_r(Imagereviews_Mappers_ListMapper::getInstance());
        $this->_view->notes = Imagereviews_Mappers_ListMapper::getInstance()->fetchAll(
            array('pageId = ?' => $this->_toasterOptions['id'])
        );
        $this->_view->pageId = $this->_toasterOptions['id'];


        return $this->_view->render('notes-list.phtml');
    }
}