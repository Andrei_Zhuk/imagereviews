<?php

class Imagereviews_Forms_AddNote extends Zend_Form
{
    /**
     * @throws Zend_Form_Exception
     */
    public function init()
    {
        $this
            ->setMethod(Zend_Form::METHOD_POST)
            ->setDecorators(
                array( 'FormElements', 'Form' )
            );

        /**
         * full name
         */
//        $this
//            ->addElement(
//                new Zend_Form_Element_Text(
//                    array(
//                        'name'       => 'fullName',
//                        'id'         => uniqid('c_'),
//                        'required'   => true,
//                        'decorators' => array(
//                            'ViewHelper', 'Label'
//                        )
//                    )
//                )
//            );

        /**
         * E-mail
         */
//        $this
//            ->addElement(
//                new Zend_Form_Element_Text(
//                    array(
//                        'name'       => 'email',
//                        'id'         => uniqid('c_'),
//                        'required'   => true,
//                        'validators' => array( 'EmailAddress' ),
//                        'decorators' => array(
//                            'ViewHelper', 'Label'
//                        )
//                    )
//                )
//            );

        /**
         * Message
         */
        $this
            ->addElement(
                new Zend_Form_Element_Textarea(
                    array(
                        'name'       => 'description',
                        'rows'       => 5,
                        'id'         => uniqid('c_'),
                        'required'   => true,
                        'decorators' => array(
                            'ViewHelper', 'Label'
                        )
                    )
                )
            );

        /**
         * positive
         */
        $this
            ->addElement(
                new Zend_Form_Element_Text(
                    array(
                        'name'       => 'imgPosX',
                        'id'         => uniqid('c_'),
                        'decorators' => array(
                            'ViewHelper', 'Label'
                        )
                    )
                )
            );

        /**
         * negative
         */
        $this
            ->addElement(
                new Zend_Form_Element_Text(
                    array(
                        'name'       => 'imgPosY',
                        'id'         => uniqid('c_'),
                        'decorators' => array(
                            'ViewHelper', 'Label'
                        )
                    )
                )
            );

        /**
         * Page id
         */
        $this
            ->addElement(
                new Zend_Form_Element_Hidden(
                    array(
                        'name'       => 'pageId',
                        'id'         => uniqid('c_'),
                        'required'   => true,
                        'decorators' => array(
                            'ViewHelper', 'Label'
                        )
                    )
                )
            );

        /**
         * Container name
         */
//        $this
//            ->addElement(
//                new Zend_Form_Element_Hidden(
//                    array(
//                        'name'       => 'container_name',
//                        'id'         => uniqid('c_'),
//                        'required'   => true,
//                        'decorators' => array(
//                            'ViewHelper', 'Label'
//                        )
//                    )
//                )
//            );

        /**
         * Container type
         */
//        $this
//            ->addElement(
//                new Zend_Form_Element_Hidden(
//                    array(
//                        'name'       => 'container_type',
//                        'id'         => uniqid('c_'),
//                        'required'   => true,
//                        'decorators' => array(
//                            'ViewHelper', 'Label'
//                        )
//                    )
//                )
//            );


        /**
         * Sign Up
         */
//        $this
//            ->addElement(
//                new Zend_Form_Element_Checkbox(
//                    'signUp',
//                    array(
//                        'name'       => 'signUp',
//                        'value'      => '1',
//                        'value'      => 'signUp',
//                        'decorators' => array(
//                            'ViewHelper', 'Label'
//                        )
//                    )
//                )
//            );

        /*$this
            ->addElement(
                new Zend_Form_Element_Text(
                    array(
                        'name'       => 'smart',
                        'id'         => uniqid('c_'),
                        'class'      => 'hide',
                        'decorators' => array(
                            'ViewHelper', 'Label'
                        )
                    )
                )
            );*/

        /**
         * Reply id
         */
//        $this
//            ->addElement(
//                new Zend_Form_Element_Hidden(
//                    array(
//                        'name'       => 'replyId',
//                        'value'      => 0,
//                        'id'         => uniqid('c_'),
//                        'required'   => true,
//                        'decorators' => array(
//                            'ViewHelper', 'Label'
//                        )
//                    )
//                )
//            );

    }
}