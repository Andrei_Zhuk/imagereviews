CREATE TABLE IF NOT EXISTS `plugin_imagereviews_list` (
  `id`            INT(10) UNSIGNED                      NOT NULL AUTO_INCREMENT,
  `pageId`        INT(10) UNSIGNED                      NOT NULL DEFAULT '0',
  `imgPosX`       INT(10) UNSIGNED                      NOT NULL DEFAULT '0',
  `imgPosY`       INT(10) UNSIGNED                      NOT NULL DEFAULT '0',
  `description`       TEXT
                  COLLATE utf8_unicode_ci               NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
)
  ENGINE =InnoDB
  DEFAULT CHARSET =utf8
  COLLATE =utf8_unicode_ci;