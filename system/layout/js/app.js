document.addEventListener('DOMContentLoaded', function () {

// begin: helper functions
    var closest = function (el, fn) {
        return el && (fn(el) ? el : closest(el.parentNode, fn));
    };

    function _arrayToQueryString(array_in) {
        var out = new Array();
        for (var key in array_in) {
            out.push(key + '=' + encodeURIComponent(array_in[key]));
        }
        return out.join('&');
    }

    /**
     *
     * @param url
     * @param type
     * @param data
     * @param callback
     */
    function httpRequest(url, type, data, callback) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open(type, (type == 'GET' && type != "") ? url + '?' + _arrayToQueryString(data) : url, true);
        //xmlHttp.setRequestHeader('Access-Control-Allow-Origin', '*');
//            xmlHttp.setRequestHeader('Accept', 'application/json, text/javascript');
//            xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        xmlHttp.send(data);
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4) {
                if (xmlHttp.status == 200) {
                    if (callback) {
                        callback(xmlHttp.responseText);
                    }
                }
            }
        }
    }


    function loadReviewsList(pId) {
        if (pId === undefined) pId = document.querySelector('.image-reviews-list').getAttribute('data-pid');
        httpRequest(
            document.getElementById('website_url').value + 'plugin/imagereviews/run/loadList',
            'GET',
            {pageId: pId}, function (response) {
                response = JSON.parse(response);
                document.querySelector('.image-reviews-list').innerHTML = response.responseList;
            });

    }

    loadReviewsList();

    function loadPointsList(pId) {
        if (pId === undefined) pId = document.querySelector('.image-points-list').getAttribute('data-pid');
        httpRequest(document.getElementById('website_url').value + 'plugin/imagereviews/run/loadPointsList',
            'GET', {pageId: pId}, function (response) {
                response = JSON.parse(response);
                document.querySelector('.image-points-list').innerHTML = response.responseList;
            });
    }

    loadPointsList();


    function multipleEventListener(event, element, callback, parent) {
        var theParent = document.querySelector(parent);
        theParent.addEventListener(event, checkElement, false);

        function checkElement(e) {
            if (e.target.classList.contains(element)) {
                callback(e.target);
            }
            e.preventDefault();
        }
    }

    var pointPos = {};

    document.querySelector('.box-image-reviews').addEventListener('click', function (event) {
        if (!event.target.matches('.point')) {

            if (document.querySelector('.point[href="#"]')) {
                document.querySelector('.point[href="#"]')
                    .parentElement.removeChild(document.querySelector('.point[href="#"]'));
            }

            // create point
            var point = document.createElement('A');
            point.className = 'point';
            point.href = '#';
            pointPos = {};
            pointPos.x = point.style.left = 100 * (event.layerX - 5) / this.offsetWidth + '%';
            pointPos.y = point.style.top = 100 * (event.layerY - 5) / this.offsetHeight + '%';
            this.querySelector('.image-points-list').appendChild(point);

            document.querySelector('#new-note').classList.remove('hide');
            // set data to the form
            document.querySelector('[name="imgPosX"]').value = pointPos.x;
            document.querySelector('[name="imgPosY"]').value = pointPos.y;
        }
    });

    document.querySelector('.action-add-note').addEventListener('click', function (e) {
        e.preventDefault();

        var btn = this;
        btn.setAttribute('disabled', 'disabled');
        var form = closest(btn, function (el) {
            return el.tagName.toLowerCase() === 'form';
        });
        var formData = new FormData(form);
        var formAction = form.getAttribute('action');
        httpRequest(formAction, 'POST', formData, function (response) {
            btn.removeAttribute('disabled');
//                response = JSON.parse(response);
//                showMessage(response.responseText, response.error, 5000);
            form.querySelector('[name="description"]').value = '';
            form.classList.add('hide');
            loadReviewsList();
            loadPointsList();
        });

    });

    /**
     *
     */
    multipleEventListener('click', 'ir-remove', function (element) {
        var wrap = closest(element, function (el) {
            return el.className === 'note';
        });
        var id = wrap.getAttribute('data-note-id');
        showConfirm(
            'Do you want to delete this note?',
            function () {
                httpRequest(
                    'api/imagereviews/notes/',
                    'DELETE',
                    JSON.stringify({id: id}),
                    function (response) {
                        wrap.parentElement.removeChild(wrap);
                        var el = document.querySelector('.point-' + id);
                        el.parentElement.removeChild(el);
                    })
            }
        );
    }, '.image-reviews-list');


    multipleEventListener('click', 'ir-edit', function (element) {
        var wrap = closest(element, function (el) {
            return el.className === 'note';
        });

        wrap.querySelector('.-desc').setAttribute('contenteditable', 'true');
        wrap.querySelector('.-desc').onfocusout = function () {
            wrap.querySelector('.-desc').setAttribute('contenteditable', 'false');

            var form = document.getElementById('new-note');
            var formData = new FormData(form);
            formData.append('id', wrap.getAttribute('data-note-id'));
            formData.set('description', wrap.querySelector('.-desc').innerHTML);

            var formAction = form.getAttribute('action');
            httpRequest(formAction, 'POST', formData, function (response) {
                //btn.removeAttribute('disabled');
                console.log(response);
                response = JSON.parse(response);
                showMessage(response.responseText, response.error, 5000);
                //form.querySelector('[name="description"]').value = '';
                //form.classList.add('hide');
                loadReviewsList();
                loadPointsList();
            });
        };
    }, '.image-reviews-list');


//        $('#new-note').on('click', 'button', function (e) {
//
//            console.log(document.body);
//            e.preventDefault();
//
//            var reviewId = $(this).parent().data('id');
//            var action = $(this).attr('class');
//
//            $.post($('#website_url').val() + 'plugin/pagerating/run/' + action, {reviewId: reviewId},
//                function (response) {
//                    if (action == 'delete') {
//                        $('#pagerating-review-' + reviewId).remove();
//                    }
//                    if (action == 'publish') {
//                        $('#pagerating-review-' + reviewId + ' .publish').remove();
//                    }
//                }
//            );
//        });

}, false);



