<?php

class Imagereviews extends Tools_Plugins_Abstract
{

    protected $_request;
    protected $_view;


    /**
     * Init method.
     *
     * Use this method to init your plugin's data and variables
     * Use this method to init specific helpers, view, etc...
     */
    protected function _init()
    {
        $this->_layout = new Zend_Layout();
        $this->_layout->setLayoutPath(Zend_Layout::getMvcInstance()->getLayoutPath());

        $this->_view = new Zend_View(array('scriptPath' => __DIR__ . '/views'));
        $this->_view->setHelperPath(APPLICATION_PATH . '/views/helpers/');
        $this->_view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
        if (($scriptPaths = $this->_layout->getView()->getScriptPaths()) !== false) {
            $this->_view->setScriptPath($scriptPaths);
        }
        $this->_view->setScriptPath(dirname(__FILE__) . '/system/views/');

        $this->_isAdmin = Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_CONTENT);
        $this->_isUser = $this->_sessionHelper->getCurrentUser()->getRoleId() != Tools_Security_Acl::ROLE_GUEST ? true : false;
        //$this->_isSAdmin = $this->_sessionHelper->getCurrentUser()->getRoleId() == Tools_Security_Acl::ROLE_SUPERADMIN ? true : false;
        $this->_layout->getView()->headScript()->appendFile(
            $this->_websiteUrl . 'plugins/imagereviews/system/layout/js/app.js'
        );
    }


    public function afterController()
    {
//        $acl = Zend_Registry::get('acl');
//        $acl->deny(Tools_Security_Acl::ROLE_MEMBER, self::RESOURCE_MEDIA);
//        $acl->allow(self::ROLE_MEMBER, Tools_Security_Acl::RESOURCE_MEDIA);
//        Zend_Registry::set('acl', $acl);
        //$config = Application_Model_Mappers_ConfigMapper::getInstance()->getConfig();

    }

    public function addNoteAction()
    {
        $message = array();
        if ($this->_request->isPost()) {
            $form = new Imagereviews_Forms_AddNote();
            if ($form->isValid($this->_request->getPost())) {
                $note = new Imagereviews_Models_ListModel($form->getValues());


                // begin: get config params
//                $params = Zend_Json::decode(
//                    Comments_Mappers_Configuration::getInstance()->findByNameAndType(
//                        $comment->getContainerName(),
//                        $comment->getContainerType()
//                    )->getParams()
//                );
//                if (is_array($params) && !empty($params)) {
//                    foreach ($params as $key => $value) {
//                        $this->_view->$key = $value;
//                    }
//                }
                // end;
//                $spamMapper = Comments_Mappers_Spam::getInstance()->findByMessage($note->getMessage());
//                if ($spamMapper) {
//                    $message = array(
//                        'responseText' => $this->_translator->translate(
//                            "This message has been flagged as spam! Maybe you're not real?"
//                        ),
//                        'class'        => 'error',
//                        'error'        => true,
//                    );
//                } else {
                // if it is user
//                    if ($this->_isUser) {
//
//                        $note->setUserId($this->_sessionHelper->getCurrentUser()->getId());
//                        $note->setEmail($this->_sessionHelper->getCurrentUser()->getEmail());
//                        $note->setFullName($this->_sessionHelper->getCurrentUser()->getFullName());
//                    }

                // manipulate with fields
//                $note->setImgPosX(0);
//                $note->setImgPosY(0);

                if($this->_request->getParam('id')){
                    $note = Imagereviews_Mappers_ListMapper::getInstance()->find($this->_request->getParam('id'));
                }
                $note->setPageId($this->_request->getParam('pageId'));

                $note->setDescription(
                    str_replace('$', '&dollar;', strip_tags($this->_request->getParam('description')))
                );

                // save comment
                $result = Imagereviews_Mappers_ListMapper::getInstance()->save($note);
//                return $result;
                if ($result) {
                    $message = array(
                        'responseText' => $this->_translator->translate(
                            "Thanks! Your comment will appear after moderation."
                        ),
                        'class' => 'success',
                        'error' => false,
                    );
//                        if ($this->_isAdmin || ($this->_isUser && $params['noModerate'])) {
//                            $message = array(
//                                'responseText' => $this->_translator->translate(
//                                    "Thanks! Your comment has been posted."
//                                ),
//                                'class'        => 'success',
//                                'error'        => false,
//                            );
//                        } else {
//                            $message = array(
//                                'responseText' => $this->_translator->translate(
//                                    "Thanks! Your comment will appear after moderation."
//                                ),
//                                'class'        => 'success',
//                                'error'        => false,
//                            );
//                        }
                }
//                }


//            } else {
//                $message = array(
//                    'responseText' => $this->_translator->translate(
//                        "Something went wrong! Please check data in the fields."
//                    ),
//                    'class'        => 'error',
//                    'error'        => true,
//                );
//            }

                echo json_encode($message);
            }
        }
    }


    public function createProjectAction()
    {
        if (Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_CONTENT)) {
//            $containerName = filter_var($this->_request->getParam('container'), FILTER_SANITIZE_STRING);
            $pageId = filter_var($this->_request->getParam('pageId'), FILTER_SANITIZE_NUMBER_INT);
//            $type          = ($pageId) ? Application_Model_Models_Container::TYPE_REGULARCONTENT : Application_Model_Models_Container::TYPE_STATICCONTENT;
//            $container     = Application_Model_Mappers_ContainerMapper::getInstance()->findByName(
//                $containerName,
//                $pageId,
//                $type
//            );
//            $ioData        = array();
//
//            if ($container instanceof Application_Model_Models_Container) {
//                $ioData = Zend_Json::decode($container->getContent());
//            }
//
//            // assign view variables
//            $this->_view->content       = ($container instanceof Application_Model_Models_Container) ? explode(
//                ':',
//                $container->getContent()
//            ) : '';
            $this->_view->pageId = $pageId;
//            $this->_view->containerName = $containerName;
//            $this->_view->typeList      = $this->_typeList;
//            $this->_view->link          = isset($ioData['link']) ? $ioData['link'] : '';
//            $this->_view->text          = isset($ioData['text']) ? $ioData['text'] : '';
//            $this->_view->title         = isset($ioData['title']) ? $ioData['title'] : '';
//            $this->_view->classList     = isset($ioData['class']) ? $ioData['class'] : '';
//            $this->_view->target        = isset($ioData['target']) ? $ioData['target'] : 0;
            $this->_view->websiteUrl = $this->_seotoasterData['websiteUrl'];
//            $this->_view->isSAdmin       = $this->_isSAdmin;
//            $this->_view->pagesList     = Application_Model_Mappers_PageMapper::getInstance()->fetchAll();

            echo $this->_view->render('createProject.phtml');
        }
    }

    public function loadPointsListAction()
    {
        $pId = filter_var($this->_request->getParam('pageId'), FILTER_SANITIZE_NUMBER_INT);
        $points = Imagereviews_Mappers_ListMapper::getInstance()->fetchAll(
            array('pageId = ?' => $pId)
        );

        $this->_view->points = $points;

        $result = $this->_view->render('widget.points.list.phtml');

        $message = array(
            'responseList' => $result,
            'class' => 'success',
            'error' => true,
        );
        echo json_encode($message);
    }


    public function loadListAction()
    {
        $pid = filter_var($this->_request->getParam('pageId'), FILTER_SANITIZE_NUMBER_INT);

        // begin: get configs
//        $configMapper = Comments_Mappers_Configuration::getInstance()->findByNameAndType(
//            $container_name,
//            $container_type
//        );
//        $configParams = Zend_Json::decode($configMapper->getParams());
        // end

        $comments = Imagereviews_Mappers_ListMapper::getInstance()->fetchAll(
            array('pageId = ?' => $pid)
        );

//        print_r($comments);

//        $dateFormat = !empty($configParams['dateFormat']) ? $configParams['dateFormat'] : $dateFormat = self::DATE_FORMAT;
//        foreach ($comments as &$comment) {
//            print_r($comment);
//        }

        $this->_view->notes = $comments;

//
//        $this->_view->isAdmin       = $this->_isAdmin;
//        $this->_view->list          = $comments;
//        $this->_view->fullName      = $this->_sessionHelper->getCurrentUser()->getFullName();
//        $this->_view->email         = $this->_sessionHelper->getCurrentUser()->getEmail();
//        $this->_view->containerName = $container_name;
//        $this->_view->containerType = $container_type;
//        $this->_view->pageId        = $pid;

        $result = $this->_view->render('widget.reviews.list.phtml');
        //print_r($result);
        $message = array(
            'responseList' => $result,
            'class' => 'success',
            'error' => true,
        );
        echo json_encode($message);
    }


}